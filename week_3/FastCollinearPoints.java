/******************************************************************************
 *  Compilation:  FastCollinearPoints.java
 *  Execution:    java FastCollinearPoints
 *  Dependencies: LineSegment, Point
 *
 *  Given 4 points, checks whether they all lie on the same line segment,
 *  returning all such line segments.
 *
 ******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MergeX;

public class FastCollinearPoints {

    /**
     * Linesegment Counter
     */
    private int segments;
    /**
     * List of line segments
     */
    private LineSegment[] lineSegments;

    /**
     * Copy the point array elments in other array.
     */
    private static Point[] copyElements(Point[] fromPoints, Point[] toPoints, int from, int to) {
        for (int i = from; i < to; i++) {
            validateNull(fromPoints[i]);
            toPoints[i] = fromPoints[i];
        }
        return toPoints;
    }

    /**
     * Raise IlegallArgument exception if Point's array is empty
     *
     * @param Point[] points Array of points
     */
    private static void validateNull(Object points) {
        if (points == null) {
            throw new IllegalArgumentException("Point's array must have elements");
        }
    }

    /**
     * Calculate slope for the given two points.
     * Raise an IllegalArgumentException when the points are the same.
     */
    private static double calculateSlope(Point p1, Point p2) {
        Double slope = p1.slopeTo(p2);
        if (slope == Double.NEGATIVE_INFINITY) {
            throw new IllegalArgumentException();
        }
        return slope;
    }

    /**
     * Raise an IllegalArgumentException when the points are the same.
     */
    private static void validateDuplicate(Point p1, Point p2) {
        if (p1.compareTo(p2) == 0) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Clone and check if there are duplicated elements.
     * Raise an Illegalargumentexception when there are duplicated elements.
     */
    private static Point[] cloneAndvalidateDuplicates(Point[] fromPoints) {
        int maxElements = fromPoints.length;
        int j = 0;
        Point[] toPoints = new Point[maxElements];
        for (int i = 1; i < maxElements; i++) {
            validateDuplicate(fromPoints[j], fromPoints[i]);
            toPoints[j] = fromPoints[j];
            j++;
        }
        toPoints[j] = fromPoints[j];
        return toPoints;
    }


    /**
     * Return a linesegment object when it doesn't exist in the existedLS
     */
    private static LineSegment buildLineSegment(Point from, Point to, List<LineSegment> existedLS) {
        LineSegment ls = new LineSegment(from, to);
        for (LineSegment els : existedLS) {
            if (els.toString().equals(ls.toString())) {
                return null;
            }
        }
        return ls;
    }

    /**
     * Checks the min element between two points
     */
    private static Point min(Point p1, Point p2) {
        if (p1.compareTo(p2) < 0) {
            return p1;
        }
        return p2;
    }

    /**
     * Checks the min element between two points
     */
    private static Point max(Point p1, Point p2) {
        if (p1.compareTo(p2) < 0) {
            return p2;
        }
        return p1;
    }

    /**
     * Finds all line segments containing 4 points
     *
     * @param points[] Array of points
     */
    public FastCollinearPoints(Point[] points) {
        validateNull(points);
        populateLineSegments(copyElements(points, new Point[points.length], 0, points.length));
    }

    /**
     * Populate LineSegments according to the line segment must contain at least 4
     * (or more) points and avoid repeating it.
     */
    private void populateLineSegments(Point[] points) {
        Point p, q, r, min, max;
        Point[] myPoints, clonedPoints;
        double slope1, slope2;
        boolean isSlope;
        int maxElements, limitVal, k, j;
        List<LineSegment> existedLS = new ArrayList<LineSegment>();
        maxElements = points.length;
        limitVal = maxElements - 1;
        MergeX.sort(points);
        clonedPoints = cloneAndvalidateDuplicates(points);

        for (int i = 0; i < maxElements; i++) {
            p = points[i];
            min = p;
            max = p;
            MergeX.sort(clonedPoints, p.slopeOrder());
            j = 1;

            while (j < limitVal) {
                k = 1;
                q = clonedPoints[j];
                min = min(min, q);
                max = max(max, q);
                slope1 = calculateSlope(p, q);

                do {
                    r = clonedPoints[j + k];
                    slope2 = calculateSlope(p, r);
                    isSlope = Double.compare(slope1, slope2) == 0;
                    if (isSlope) {
                        k++;
                        min = min(min, r);
                        max = max(max, r);
                    }
                } while (j + k < maxElements && isSlope); 

                if (k > 2) {
                    LineSegment ls = buildLineSegment(min, max, existedLS);
                    if (ls != null) {
                        existedLS.add(ls);
                        this.segments++;
                    }
                }

                j = j + k;
                min = p;
                max = p;
            }
        }
        this.lineSegments = existedLS.toArray(new LineSegment[0]);
    }

    /**
     * Returns the number of line segments
     */
    public int numberOfSegments() {
        return this.segments;
    }

    /**
     * Return the line segments
     */
     public LineSegment[] segments() {
         return lineSegments.clone();
     }

     /**
     * This client program takes the name of an input file as a command-line argument;
     * read the input file (in the format specified below); prints to standard output
     * the line segments that your program discovers, one per line; and draws to standard
     * draw the line segments.
     *
     */
    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            // StdOut.println(p);
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        FastCollinearPoints collinear = new FastCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
