/******************************************************************************
 *  Compilation:  javac BruteCollinearPoints.java
 *  Execution:    java BruteCollinearPoints
 *  Dependencies: LineSegment, Point
 *  
 *  Given 4 points, checks whether they all lie on the same line segment,
 *  returning all such line segments. It's calculated using 2N + N^4
 *
 ******************************************************************************/

import java.util.Arrays;

import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.In;

public class BruteCollinearPoints {

    /**
     * Linesegment Counter
     */
    private int segments;
    /**
     * List of line segments
     */
    private LineSegment[] lineSegments;

    /**
     * Raise IlegallArgument exception if Point's array is empty
     *
     * @param Point[] points Array of points
     */
    private static void validateNull(Object points) {
        if (points == null) {
            throw new IllegalArgumentException("Point's array must have elements");
        }
    }

    /**
     * Copy the point array elments in other array.
     */
    private static Point[] copyElements(Point[] fromPoints, Point[] toPoints, int from, int to) {
        for (int i = from; i < to; i++) {
            validateNull(fromPoints[i]);
            toPoints[i] = fromPoints[i];
        }
        return toPoints;
    }

    /**
     * Calculate slope for the given two points.
     * Raise an IllegalArgumentException when the points are the same.
     */
    private static double calculateSlope(Point p1, Point p2) {
        Double slope = p1.slopeTo(p2);
        if (slope == Double.NEGATIVE_INFINITY) {
            throw new IllegalArgumentException();
        }
        return slope;
    }

    /**
     * Finds all line segments containing 4 points
     *
     * @param points[] Array of points
     */
    public BruteCollinearPoints(Point[] points) {
        validateNull(points);
        populateLineSegments(points);
    }

    /**
     * Poputalte LineSegments based on the given Point arrays
     */
    private void populateLineSegments(Point[] points) {
        Point p, q, r, s;
        double slope1, slope2, slope3;
        Point[] clonedPoints, selectedPoints;
        Stack<LineSegment> existedSegments = new Stack<LineSegment>();
        int maxElements = points.length;
        clonedPoints = copyElements(points, new Point[maxElements], 0, maxElements);
        for (int i = 0; i < maxElements - 1; i++) {
            p = clonedPoints[i];
            for (int j = i + 1; j < maxElements; j++) {
                q = clonedPoints[j];
                slope1 = calculateSlope(p, q);
                for (int k = j + 1; k < maxElements; k++) {
                    r = clonedPoints[k];
                    slope2 = calculateSlope(p, r);
                    for (int m = k + 1; m < maxElements && Double.compare(slope1, slope2) == 0; m++) {
                        s = clonedPoints[m];
                        slope3 = calculateSlope(p, s);
                        if (Double.compare(slope2, slope3) == 0) {
                            selectedPoints = new Point[]{p, q, r, s};
                            Arrays.sort(selectedPoints);
                            LineSegment lineSegment = new LineSegment(selectedPoints[0], selectedPoints[3]);
                            this.segments++;
                            existedSegments.push(lineSegment);
                        }
                    }
                }
            }
        }
        this.lineSegments = new LineSegment[this.segments];
        for (int i = 0; i < this.segments; i++) {
            this.lineSegments[i] = existedSegments.pop();
        }
    }

    /**
     * Returns the number of line segments
     */
    public int numberOfSegments() {
        return this.segments;
    }

    /**
     * Return the line segments
     */
    public LineSegment[] segments() {
        return this.lineSegments.clone();
    }

    /**
     * This client program takes the name of an input file as a command-line argument;
     * read the input file (in the format specified below); prints to standard output
     * the line segments that your program discovers, one per line; and draws to standard
     * draw the line segments.
     *
     */
    public static void main(String[] args) {

        // read the n points from a file
        In in = new In(args[0]);
        int n = in.readInt();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
        }

        // draw the points
        StdDraw.enableDoubleBuffering();
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        for (Point p : points) {
            // StdOut.println(p);
            p.draw();
        }
        StdDraw.show();

        // print and draw the line segments
        BruteCollinearPoints collinear = new BruteCollinearPoints(points);
        for (LineSegment segment : collinear.segments()) {
            StdOut.println(segment);
            segment.draw();
        }
        StdDraw.show();
    }
}
