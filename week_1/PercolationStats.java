
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {

    private static final double CONFIDENCE_INTERVAL = 1.96;

    private double threshold[];
    private int trials;
    private double xmean;
    private double xstddev;

    // perform independent trials on an n-by-n grid
    public PercolationStats(int n, int trials) {
        this.validateBoundary(n, trials);
        this.threshold = new double[trials];

        this.simulate(n, trials);

        this.xmean = StdStats.mean(this.threshold);
        this.xstddev = StdStats.stddev(this.threshold);
        this.trials = trials;
    }

    private void validateBoundary(int n, int trials) {
        if ((n <= 0) || (trials <= 0)) {
            throw new IllegalArgumentException("N should be a positive number");
        }
    }

    private void simulate(int n, int trials) {
        double power = n * n;
        int maxRand = n + 1;
        for (int i=0; i < trials; i++) {
            Percolation perc = new Percolation(n);
            while (!perc.percolates()) {
                int row = StdRandom.uniform(1, maxRand);
                int col = StdRandom.uniform(1, maxRand);
                perc.open(row, col);
            }
            this.threshold[i] = perc.numberOfOpenSites() / power;
        }
    }

    // sample mean of percolation threshold
    public double mean() {
        return this.xmean;
    }

    // sample standard deviation of percolation threshold
    public double stddev() {
        return this.xstddev;
    }

    // low endpoint of 95% confidence interval
    public double confidenceLo() {
        return this.xmean - (CONFIDENCE_INTERVAL * this.xstddev) / Math.sqrt(this.trials);
    }

    // high endpoint of 95% confidence interval
    public double confidenceHi() {
        return this.xmean + (CONFIDENCE_INTERVAL * this.xstddev) / Math.sqrt(this.trials);
    }

   // test client (see below)
   public static void main(String[] args) {
       int n = Integer.parseInt(args[0]);
       int trials = Integer.parseInt(args[1]);
       PercolationStats stats = new PercolationStats(n, trials);
       StdOut.println("mean                    = " + stats.mean());
       StdOut.println("stddev                  = " + stats.stddev());
       StdOut.println("95% confidence interval = " + stats.confidenceLo() + 
                          ", " + stats.confidenceHi());
   }
}
