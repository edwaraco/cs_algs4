
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;


public class Percolation {

    private static final int TOP = 0;
    private final int n;
    private final int bottom;
    private WeightedQuickUnionUF uf;
    private WeightedQuickUnionUF percolationRel;
    private byte site[]; // 0-> closed; 1 -> open
    private int openSites;

    // creates n-by-n grid, with all sites initially blocked
    public Percolation(int n) {
        this.validateN(n);
        int mat = n * n;
        this.n = n;
        this.uf = new WeightedQuickUnionUF(mat);
        this.percolationRel = new WeightedQuickUnionUF(mat);
        this.bottom = mat - 1;
        this.site = new byte[mat];
        this.openSites = 0;
    }

    private boolean isTop(int row) {
        return row - 1 == TOP;
    }

    private boolean isBottom(int row) {
        return row == this.n;
    }

    private void validateN(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("N should be a positive number");
        }
    }

    private boolean isInRange(int n) {
        return 1 <= n && n <= this.n;
    }

    private void validateBoundaries(int i, int j) {
        if (!this.isInRange(i) || !this.isInRange(j))  {
            throw new IllegalArgumentException("To both Rows and Cols should be in allowed range: (" + i + "," + j + ")");
        }
    }

    private int convert2Dto1D(int i, int j) {
        return this.n * (i - 1) + (j - 1);
    }

    private void unionFlow(int currentPos, int i, int j) {
        if (this.isOpen(i, j)) {
            int newCoord = this.convert2Dto1D(i, j);
            this.uf.union(currentPos, newCoord);
            this.percolationRel.union(currentPos, newCoord);
        }
    }

    // opens the site (row, col) if it is not open already
    public void open(int row, int col) {
        if (this.isOpen(row, col)) { return; }
        
        int currPos1D = this.convert2Dto1D(row, col);

        this.site[currPos1D] = 1;
        this.openSites++;

        if (this.isTop(row)) {
            this.uf.union(currPos1D, TOP);
            this.percolationRel.union(currPos1D, TOP);
        } else if (this.isBottom(row)) {
            this.percolationRel.union(currPos1D, this.bottom);
        }

        if (row < this.n) { this.unionFlow(currPos1D, row + 1, col); }
        if (row > 1) { this.unionFlow(currPos1D, row - 1, col); }
        if (col < this.n) { this.unionFlow(currPos1D, row, col + 1); }
        if (col > 1) { this.unionFlow(currPos1D, row, col - 1); }
    }

    // is the site (row, col) open?
    public boolean isOpen(int row, int col) {
        this.validateBoundaries(row, col);
        int coord1D = this.convert2Dto1D(row, col);
        return this.site[coord1D] == 1;
    }

    // is the site (row, col) full?
    public boolean isFull(int row, int col) {
        if (!this.isOpen(row, col)) { return false; }

        int coord1D = this.convert2Dto1D(row, col);
        return this.uf.find(TOP) == this.uf.find(coord1D);
    }

    // returns the number of open sites
    public int numberOfOpenSites() {
        return this.openSites;
    }

    // does the system percolate?
    public boolean percolates() {
        return this.percolationRel.find(TOP) == this.percolationRel.find(this.bottom);
    }
    

    // test client (optional)
    public static void main(String[] args) {
        
    }
}
