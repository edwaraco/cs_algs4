/******************************************************************************
 *  Compilation:  javac Board.java
 *  Execution:    java Board
 *  Dependencies:
 *
 *  Models an n-by-n board with sliding tiles used for solving the N-puzzle
 *
 ******************************************************************************/
import java.util.Iterator;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class Board {
    private final int dimension;
    private final int[][] tiles;
    private Board twinBoard;
    private int[] zeroPos;
    private int hammingDistance;
    private int manhattanDistance;

    private class Neighbors implements Iterable<Board> {
        private Stack<Board> neighbors;

        public Neighbors(int[][] base, int[] zeroPosition) {
            int dimension = base.length;
            int zeroPosRow = zeroPosition[0];
            int zeroPosCol = zeroPosition[1];
            this.neighbors = new Stack<>();
            int[][] a = {{zeroPosRow - 1, zeroPosCol},
                         {zeroPosRow + 1, zeroPosCol},
                         {zeroPosRow, zeroPosCol - 1},
                         {zeroPosRow, zeroPosCol + 1}};
            for (int i = 0; i < a.length; i++) {
                if (0 <= a[i][0] && a[i][0] < dimension &&
                    0 <= a[i][1] && a[i][1] < dimension) {
                    int[][] tiles = cloneMatrix(base);
                    this.neighbors.push(new Board(exchange(tiles, zeroPosRow, zeroPosCol, a[i][0], a[i][1])));
                }
            }
        }

        public Iterator<Board> iterator() { return new NeighborsIterator(); }

        private class NeighborsIterator implements Iterator<Board> {
            public boolean hasNext() { return !neighbors.isEmpty(); }

            public Board next() { return neighbors.pop(); }

            public void remove() { throw new UnsupportedOperationException(); }
        }

    }

    private static String tilesToString(int[][] tiles, int dimension) {
        StringBuilder boardStr = new StringBuilder(Integer.toString(dimension));
        boardStr.append(System.lineSeparator());
        for (int i = 0; i < dimension; i++) {
            boardStr.append(Integer.toString(tiles[i][0]));
            for (int j = 1; j < dimension; j++) {
                boardStr.append(" ");
                boardStr.append(Integer.toString(tiles[i][j]));
            }
            boardStr.append(System.lineSeparator());
        }
        return boardStr.toString();
    }

    private static int[][] cloneMatrix(int[][] tiles) {
        int dimension = tiles.length;
        int[][] newTiles = new int[dimension][dimension];
        for (int i=0; i < dimension; i++) {
            System.arraycopy(tiles[i], 0, newTiles[i], 0, dimension);
        }
        return newTiles;
    }

    private static int[][] exchange(int[][] exchd, int fromRow, int fromCol, int toRow, int toCol) {
        int swap = exchd[fromRow][fromCol];
        exchd[fromRow][fromCol] = exchd[toRow][toCol];
        exchd[toRow][toCol] = swap;
        return exchd;
    }

    private static Board createTwin(int[][] tiles, int dimension) {
        int fromRow, fromCol, toRow, toCol;
        do {
            fromRow = StdRandom.uniform(dimension);
            fromCol = StdRandom.uniform(dimension);
            toRow   = StdRandom.uniform(dimension);
            toCol   = StdRandom.uniform(dimension);
        } while(tiles[fromRow][fromCol] == 0 || tiles[toRow][toCol] == 0 ||
                tiles[fromRow][fromCol] == tiles[toRow][toCol]);

        int[][] newTiles = cloneMatrix(tiles);
        return new Board(exchange(newTiles, fromRow, fromCol, toRow, toCol));
    }

    /**
     * Raise IlegallArgument exception if Point's array is empty
     *
     * @param Point[] points Array of points
     */
    private static void validateNull(Object points) {
        if (points == null) {
            throw new IllegalArgumentException("Point's array must have elements");
        }
    }

    /**
     * Create a board from an n-by-n array of tiles,
     * where tiles[row][col] = tile at (row, col)
     */
    public Board(int[][] tiles) {
        validateNull(tiles);
        int tileValue;
        this.dimension = tiles.length;
        this.tiles = new int[this.dimension][this.dimension];
        this.hammingDistance = 0;
        this.manhattanDistance = 0;
        for (int i=0; i < this.dimension; i++) {
            for (int j=0; j < this.dimension; j++) {
                tileValue = tiles[i][j];
                this.analyzeHammingDistance(i, j, tileValue);
                this.analyzeManhattanDistance(i, j, tileValue);
                this.analyzeZeroPosition(i, j, tileValue);
                this.tiles[i][j] = tileValue;
            }
        }
    }

    /**
     * Checks if the given is in the correct position.
     */
    private boolean isCorrectPosition(int row, int col, int value) {
        return ((value - 1) == (this.dimension * row) + col);
    }

    /**
     * Calculate hamming distances for the current board.
     */
    private void analyzeHammingDistance(int row, int col, int value) {
        if (value > 0 && !this.isCorrectPosition(row, col, value)) {
            this.hammingDistance++;
        }
    }

    /**
     * Calculate manhattan distance for the current board
     */
    private void analyzeManhattanDistance(int row, int col, int value) {
        if (value == 0) return;
        int realVal = (value - 1);
        int correctRow = realVal / this.dimension;
        int correctCol = realVal % this.dimension;
        int rowDistance = Math.abs(row - correctRow);
        int colDistance = Math.abs(col - correctCol);
        this.manhattanDistance += rowDistance + colDistance;
    }

    /**
     * Get zero position in the current dashboard.
     */
    private void analyzeZeroPosition(int row, int col, int value) {
        if (value == 0) {
            this.zeroPos = new int[]{row, col};
        }
    }

    /**
     * Return string representation of this board
     */
    public String toString() {
        return tilesToString(this.tiles, this.dimension);
    }

    /**
     * Returns board dimension
     */
    public int dimension() {
        return this.dimension;
    }

    /**
     * Return number of tiles out of place
     */
    public int hamming() {
        return this.hammingDistance;
    }

    /**
     * Returns sum of Manhattan distances between tiles and goal
     */
    public int manhattan() {
        return this.manhattanDistance;
    }

    /**
     * Return if this board is the goal board.
     */
    public boolean isGoal() {
        return this.hammingDistance == 0;
    }

    /**
     * Return if the given board is equals.
     */
    public boolean equals(Object y) {
        if (y == null) return false;
        if (!(y instanceof Board)) return false;

        Board boardComparator = (Board) y;
        if (this.dimension() != boardComparator.dimension()) return false;

        for (int i = 0; i < this.dimension; i++) {
            for (int j = 0; j< this.dimension; j++) {
                if (this.tiles[i][j] != boardComparator.tiles[i][j]) return false;
            }
        }

        return true;
    }

    /**
     * Return all neighboring boards
     */
    public Iterable<Board> neighbors() {
        return new Neighbors(this.tiles, this.zeroPos);
    }

    /**
     * Return a board that is obtained by exchanging any pair of tiles
     */
    public Board twin() {
        if (this.twinBoard == null) {
            this.twinBoard = createTwin(this.tiles, this.dimension);
        }
        return this.twinBoard;
    }

    // unit testing (not graded)
    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] tiles, tiles1;
        tiles = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                tiles[i][j] = in.readInt();
        Board initial = new Board(tiles);
        Board comp = new Board(tiles);
        tiles1 = new int[][]{{1, 2},{3, 4}};
        Board comp2 = new Board(tiles1);
        StdOut.println(initial);
        StdOut.println("Hamming = " + initial.hamming());
        StdOut.println("Manhattan = " + initial.manhattan());
        StdOut.println("Goal = " + initial.isGoal());
        StdOut.println("Equals = " + initial.equals(comp));
        StdOut.println("Equals invalid by dimension = " + initial.equals(comp2));
        StdOut.println("Equals invalid by null = " + initial.equals(null));
        StdOut.println("Equals invalid by instance = " + initial.equals("Hello world!"));
        StdOut.println("Zero position = " + Integer.toString(initial.zeroPos[0]) + ", " + Integer.toString(initial.zeroPos[1]));

        StdOut.println("Neighbors...");
        for (Board board : initial.neighbors())
            StdOut.println(board);

        StdOut.println("Twin...");
        StdOut.println(initial.twin());
        StdOut.println(initial.twin());
    }

}
