/******************************************************************************
 *  Compilation:  javac Solver.java
 *  Execution:    java Solver
 *  Dependencies: Board
 *
 *  A* search implementation to solve n-by-n slider puzzles.
 *
 ******************************************************************************/
import java.util.Iterator;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

public class Solver {

    private final Stack<Board> solution;
    private final int moves;

    private class BoardNode implements Comparable<BoardNode> {
        /**
         * Current board
         */
        private final Board board;
        /**
         *  Distance between the current node and the start node (represent G).
         */
        private int moves;
        /**
         * Estimated distance from the current node to the end node (Represent H - Heuristic).
         */
        private final int manhattan;
        /**
         * Total cost of the node (represent G).
         */
        private int totalMoves;

        private BoardNode previousBoard;

        public BoardNode(Board board, BoardNode previousBoard) {
            this.board = board;
            this.moves = 0;
            this.manhattan = board.manhattan();
            if (previousBoard != null) {
                this.moves = previousBoard.moves + 1;
                this.previousBoard = previousBoard;
            }
            this.totalMoves = this.moves + this.manhattan;
        }

        public int compareTo(BoardNode cmp) {
            if (this.totalMoves < cmp.totalMoves) return -1;
            if (this.totalMoves > cmp.totalMoves) return +1;
            return 0;
        }
    }

    private static int calculateMoves(BoardNode boardNode) {
        if (boardNode == null) return -1;
        return boardNode.moves;
    }

    private static Stack<Board> buildSolution(BoardNode solution) {
        if (solution == null) return null;

        Stack<Board> stackBoard = new Stack<Board>();;
        BoardNode boardNode = solution;
        do {
            stackBoard.push(boardNode.board);
            boardNode = boardNode.previousBoard;
        } while(boardNode != null);

        return stackBoard;
    }

    /**
     * Raise IlegallArgument exception if Point's array is empty
     *
     * @param Point[] points Array of points
     */
    private static void validateNull(Object points) {
        if (points == null) {
            throw new IllegalArgumentException("Point's array must have elements");
        }
    }

    /**
     * Find a solution to the initial board (using the A* algorithm)
     *
     */
    public Solver(Board initial) {
        validateNull(initial);
        BoardNode boardSolution = findSolution(initial);
        this.moves = calculateMoves(boardSolution);
        this.solution = buildSolution(boardSolution);
    }

    private BoardNode findSolution(Board initial) {
        MinPQ<BoardNode> pq = new MinPQ<>();
        pq.insert(new BoardNode(initial, null));

        MinPQ<BoardNode> tpq = new MinPQ<>();
        tpq.insert(new BoardNode(initial.twin(), null));

        BoardNode node = pq.min();
        BoardNode tNode = tpq.min();
        while (!node.board.isGoal() && !tNode.board.isGoal()) {
            node = pq.delMin();
            tNode = tpq.delMin();

            for (Board neighbor: node.board.neighbors()) {
                if (neighbor.isGoal()) {
                    return new BoardNode(neighbor, node);
                } else if (node.previousBoard == null || !node.previousBoard.board.equals(neighbor)) {
                    pq.insert(new BoardNode(neighbor, node));
                }
            }

            for (Board neighbor: tNode.board.neighbors()) {
                if (neighbor.isGoal()) {
                    return null;
                } else if (tNode.previousBoard == null || !tNode.previousBoard.board.equals(neighbor)) {
                    tpq.insert(new BoardNode(neighbor, tNode));
                }
            }
        }
        if (node.board.isGoal()) {
            return node;
        }
        return null;
    }

    /**
     * Return if the initial board is solvable
     */
    public boolean isSolvable(){
        return this.moves != -1;
    }

    /**
     * Return Min number of moves to solve initial board; -1 if unsolvable
     */
    public int moves() {
        return this.moves;
    }

    /**
     * Sequence of boards in a shortest solution; null if unsolvable
     */
    public Iterable<Board> solution() {
        return this.solution;
    }

    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] tiles = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                tiles[i][j] = in.readInt();
        Board initial = new Board(tiles);

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }
}
