
import java.util.Iterator;
import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.StdOut;

public class Deque<Item> implements Iterable<Item> {
    // Contains the first element registered
    private Node front;
    // Contains the last element registered
    private Node rear;
    // Count the elements in the dequeue
    private int elements;

    private class Node {
        Item item;
        Node prev;
        Node next;

        public Node(Item i) {
            this.item = i;
            this.prev = null;
            this.next = null;
        }
    }

    private class DequeueIteratorFront implements Iterator<Item> {
        Node current;

        public DequeueIteratorFront() {
            this.current = front;
        }

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException("Method not allowed");
        }

        public void validateHasNext() {
            if (!this.hasNext()) {
                throw new NoSuchElementException("Iterator doesn't have items");
            }
        }

        public Item next() {
            this.validateHasNext();
            Item item = this.current.item;
            this.current = this.current.prev;
            return item;
        }
    }

    private class DequeueIteratorRear implements Iterator<Item> {
        Node current;

        public DequeueIteratorRear() {
            this.current = rear;
        }

        public boolean hasNext() {
            return current != null;
        }

        public void remove() {
            throw new UnsupportedOperationException("Method not allowed");
        }

        public void validateHasNext() {
            if (!this.hasNext()) {
                throw new NoSuchElementException("Iterator doesn't have items");
            }
        }

        public Item next() {
            this.validateHasNext();
            Item item = this.current.item;
            this.current = this.current.next;
            return item;
        }
    }

    private void validateNullValues(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("You should not add null values");
        }
    }

    private void validateEmpty() {
        if (this.isEmpty()) {
            throw new NoSuchElementException("You should not remove elements from a empty dequeue");
        }
    }

    // construct an empty deque
    public Deque() {
        this.front = null;
        this.rear = null;
        this.elements = 0;
    }

    // is the deque empty?
    public boolean isEmpty() {
        return this.elements == 0;
    }

    // return the number of items on the deque
    public int size() {
        return this.elements;
    }

    // add the item to the front
    public void addFirst(Item item) {
        this.validateNullValues(item);
        Node oldFront = this.front;
        this.front = new Node(item);
        this.front.prev = oldFront;
        if (this.isEmpty()) {
            this.rear = this.front;
        } else {
            oldFront.next = this.front;
        }
        this.elements++;
    }

    // add the item to the back
    public void addLast(Item item) {
        this.validateNullValues(item);
        Node oldRear = this.rear;
        this.rear = new Node(item);
        this.rear.next = oldRear;
        if (this.isEmpty()) {
            this.front = this.rear;
        } else {
            oldRear.prev = this.rear;
        }
        this.elements++;
    }

    // remove and return the item from the front
    public Item removeFirst() {
        this.validateEmpty();
        Node front = this.front;
        this.front = front.prev;
        this.elements--;
        if (this.isEmpty()) {
            this.rear = null;
        } else {
            this.front.next = null;
        }
        return front.item;
    }

    // remove and return the item from the back
    public Item removeLast() {
        this.validateEmpty();
        Node rear = this.rear;
        this.rear = rear.next;
        this.elements--;
        if (this.isEmpty()) {
            this.front = null;
        } else {
            this.rear.prev = null;
        }
        return rear.item;
    }

    // return an iterator over items in order from front to back
    public Iterator<Item> iterator() {
        return new DequeueIteratorFront();
    }

    private Iterator<Item> rearIterator() {
        return new DequeueIteratorRear();
    }
    
    private static void addFront(Deque<Integer> dequeue, Integer element) {
        dequeue.addFirst(element);
        StdOut.println("(addFirst) " + element + " inserted, size: " + dequeue.size());
    }

    private static void removeFront(Deque<Integer> dequeue) {
        Integer element = dequeue.removeFirst();
        StdOut.println("(removeFirst) " + element + " removed, size: " + dequeue.size());
    }

    private static void addRear(Deque<Integer> dequeue, Integer element) {
        dequeue.addLast(element);
        StdOut.println("(addRear) " + element + " inserted, size: " + dequeue.size());
    }

    private static void removeRear(Deque<Integer> dequeue) {
        Integer element = dequeue.removeLast();
        StdOut.println("(removeRaer) " + element + " removed, size: " + dequeue.size());
    }

    private static void applyFrontIterator(Deque<Integer> dequeue) {
        StdOut.println("Iterating from the front");
        for (Integer s : dequeue) {
            StdOut.println(s);
        }
    }

    private static void applyRaerIterator(Deque<Integer> dequeue) {
        StdOut.println("Iterating from the rear");
        Iterator<Integer> i = dequeue.rearIterator();
        while (i.hasNext()) {
            Integer s = i.next();
            StdOut.println(s);
        }
    }
    
    // unit testing (required)
    public static void main(String[] args) {
        Deque<Integer> dequeue = new Deque<>();
        try {
            dequeue.addFirst(null);
            StdOut.println("(AddFirst) Error: It should raise an error because we can not add null values");
        } catch (IllegalArgumentException iae) {
            StdOut.println("(AddFirst): IllegalArgumentException for including null values.");
        }

        try {
            dequeue.addLast(null);
            StdOut.println("(AddLast) Error: It should raise an error because we can not add null values");
        } catch (IllegalArgumentException iae) {
            StdOut.println("(AddLast): IllegalArgumentException for including null values.");
        }

        StdOut.println("Inserting valid values");
        addRear(dequeue, 1);
        addFront(dequeue, 2);
        addFront(dequeue, 3);
        addRear(dequeue, 4);
        addRear(dequeue, 5);
        addRear(dequeue, 6);

        applyFrontIterator(dequeue);
        applyRaerIterator(dequeue);

        StdOut.println("Removing values");
        removeFront(dequeue);
        removeRear(dequeue);
        removeRear(dequeue);
        removeFront(dequeue);
        removeFront(dequeue);
        removeRear(dequeue);

        applyFrontIterator(dequeue);
        applyRaerIterator(dequeue);

        try {
            dequeue.removeFirst();
            StdOut.println("(removeFirst) Error: It should raise an error because we can not remove elements from an empty dequeue");
        } catch (NoSuchElementException nse) {
            StdOut.println("(removeFirst): NoSuchElementException for removing elements from an empty dequeue");
        }

        try {
            dequeue.removeLast();
            StdOut.println("(removeLast) Error: It should raise an error because we can not remove elements from an empty dequeue");
        } catch (NoSuchElementException nse) {
            StdOut.println("(removeLast): NoSuchElementException for removing elements from an empty dequeue");
        }
    }
}
