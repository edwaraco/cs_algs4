import java.util.Iterator;
import java.util.NoSuchElementException;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;


public class RandomizedQueue<Item> implements Iterable<Item> {

    private static final int MAX_ELEMENTS = 16;

    private Item[] queue;
    private int elements;
    private int maxE;

    private static <E> int getIndex(E[] queue) {
        int i = -1;
        int maxRand = queue.length;
        do {
            i = StdRandom.uniform(maxRand);
        } while(queue[i] == null);
        return i;
    }

    private static <E> E[] copyElements(E[] fromQueue,  E[] toQueue, int from, int to) {
        for (int i = from; i < to; i++) {
            toQueue[i] = fromQueue[i];
        }
        return toQueue;
    }

    private class RandomizedQueueIterator implements Iterator<Item> {
        private Item[] queueIt;
        private int i;

        public RandomizedQueueIterator() {
            this.queueIt = copyElements(queue, (Item[]) new Object[elements], 0, elements);
            this.i = elements;
        }

        public boolean hasNext() {
            return this.i > 0;
        }

        public void remove() {
            throw new UnsupportedOperationException("Method not allowed");
        }

        public void validateHasNext() {
            if (!this.hasNext()) {
                throw new NoSuchElementException("Iterator doesn't have items");
            }
        }

        public Item next() {
            this.validateHasNext();
            int idx = getIndex(this.queueIt);
            Item item = this.queueIt[idx];
            this.queueIt[idx] = null;
            this.i--;
            return item;
        }
    }

    private void resize(int newCapacity) {
        this.queue = copyElements(this.queue, (Item[]) new Object[newCapacity], 0, this.elements);
        this.maxE = newCapacity;
    }

    private boolean doIncrement() {
        return 0 < this.elements && this.elements == this.queue.length;
    }

    private boolean doDecrement() {
        return 0 < this.elements && this.elements == this.queue.length / 4;
    }

    private void validateNullValues(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("You should not add null values");
        }
    }

    private void validateEmpty() {
        if (this.isEmpty()) {
            throw new NoSuchElementException("You should not remove elements from a empty queue");
        }
    }

    private void reOrderQueue(int index) {
        for (int i = index; i < this.elements; i++) {
            this.queue[i] = this.queue[i + 1];
        }
        this.queue[this.elements] = null;
    }

    // construct an empty randomized queue
    public RandomizedQueue() {
        this.queue = (Item[]) new Object[MAX_ELEMENTS];
        this.elements = 0;
        this.maxE = MAX_ELEMENTS;
    }

    // is the randomized queue empty?
    public boolean isEmpty() {
        return this.elements == 0;
    }

    // return the number of items on the randomized queue
    public int size() {
        return this.elements;
    }

    // add the item
    public void enqueue(Item item) {
        this.validateNullValues(item);
        this.queue[this.elements] = item;
        this.elements++;
        if (this.doIncrement()) {
            this.resize(this.maxE * 2);
        }
    }

    // remove and return a random item
    public Item dequeue(){
        this.validateEmpty();
        int index = getIndex(this.queue);
        Item item = this.queue[index];
        this.reOrderQueue(index);
        this.elements--;
        if (this.doDecrement()) {
            this.resize(this.queue.length / 2);
        }
        return item;
    }

    // return a random item (but do not remove it)
    public Item sample() {
        this.validateEmpty();
        int index = getIndex(this.queue);
        return this.queue[index];
    }

    private Item get(int i) {
        return this.queue[i];
    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator() {
        return new RandomizedQueueIterator();
    }

    private static void rqDequeue(RandomizedQueue<Integer> randqueue) {
        Integer element = randqueue.dequeue();
        StdOut.println("(dequeue) " + element + " removed, size: " + randqueue.size());
    }

    private static void rqEnqueue(RandomizedQueue<Integer> randqueue, Integer element) {
        randqueue.enqueue(element);
        StdOut.println("(enqueue) " + element + " inserted, size: " + randqueue.size());
    }

    private static void printQueue(RandomizedQueue<Integer> randqueue) {
        int maxE = randqueue.maxE;
        StdOut.println("Printing queue");
        for (int i = 0; i < maxE; i++) {
            StdOut.println(randqueue.get(i));
        }
    }

    private static void printRandomizedQueue(RandomizedQueue<Integer> randqueue) {
        StdOut.println("Printing Randomized queue");
        for (Integer e: randqueue) {
            StdOut.println(e);
        }
    }

    // unit testing (required)
    public static void main(String[] args) {
        RandomizedQueue<Integer> rq = new RandomizedQueue<Integer>();
        for (int i = 1; i < 17; i++) {
            rqEnqueue(rq, i);
        }
        StdOut.println("(rand_enqueue) max_elements: " + rq.maxE);
        printQueue(rq);
        printRandomizedQueue(rq);
        for (int i = 0; i < 8; i++) {
            rqDequeue(rq);
        }
        printQueue(rq);
        StdOut.println("(rand_enqueue) max_elements: " + rq.maxE);
    }
}
