/******************************************************************************
 *  Compilation:  javac KdTree.java
 *  Execution:    java KdTree
 *  Dependencies: Point2D, RectHV
 *
 *  A 2d-tree is a generalization of a BST to two-dimensional keys.
 *  The idea is to build a BST with points in the nodes,
 *  using the x- and y-coordinates of the points as keys in strictly
 *  alternating sequence.
 *
 ******************************************************************************/
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

public class KdTree {

    private static class Node {
        /**
         * The point
         */
        private Point2D p;
        /**
         * An axis-aligned rectangle in the unit square,
         * which encloses all of the points in its subtree.
         * Used to try to find rectangle intersections.
         */
        private RectHV rect;
        /**
         * The left/bottom subtree
         */
        private Node lb;
        /**
         * The right/top subtree
         */
        private Node rt;

        private Node(Point2D p, RectHV rect) {
            this.p = p;
            this.rect = rect;
        }

        private double delta(Point2D p, boolean vertical) {
            if (vertical) return p.x() - this.p.x();
            return p.y() - this.p.y();
        }

        private int compareTo(Point2D p, boolean vertical) {
            if (this.p.compareTo(p)==0) { return 0; }
            double delta = this.delta(p, vertical);
            if (delta <= 0) return -1;
            return 1;
        }

        private RectHV buildParentRect(Point2D p, boolean vertical) {
            double xmin = this.rect.xmin();
            double ymin = this.rect.ymin();
            double xmax = this.rect.xmax();
            double ymax = this.rect.ymax();
            if (vertical) {
                int comp = Point2D.X_ORDER.compare(p, this.p);
                if (comp < 0) {
                    xmax = this.p.x();
                } else {
                    xmin = this.p.x();
                }
            } else {
                int comp = Point2D.Y_ORDER.compare(p, this.p);
                if (comp < 0) {
                    ymax = this.p.y();
                } else {
                    ymin = this.p.y();
                }
            }
            return new RectHV(Math.min(xmin, xmax),
                              Math.min(ymin, ymax),
                              Math.max(xmin, xmax),
                              Math.max(ymin, ymax));
        }
    }

    private Node root;
    private int elements;

    /**
     * Raise IlegallArgument exception if the object is null
     *
     * @param point Object
     */
    private static void validateNull(Object point) {
        if (point == null) {
            throw new IllegalArgumentException("Object is required");
        }
    }

    /**
     * Construct an empty set of points
     */
    public KdTree() {
        this.root = null;
        this.elements = 0;
    }

    /**
     * Is the set empty?
     */
    public boolean isEmpty() {
        return this.elements == 0;
    }

    /**
     * Number of points in the set
     */
    public int size() {
        return this.elements;
    }

    /**
     * Insert a new point using a recursion method to do it.
     */
    private Node add(Node node, RectHV rect, Point2D p, boolean vertical) {
        if (node == null) {
            this.elements++;
            return new Node(p, rect);
        }
        int comp = node.compareTo(p, vertical);
        if (comp == 0) return node;

        RectHV newRect = node.buildParentRect(p, vertical);

        if (comp < 0) {
            // StdOut.println("Insert " + p + " on the left side: " + vertical);
            node.lb = add(node.lb, newRect, p, !vertical);
        } else {
            // StdOut.println("Insert " + p + " on the right side: " + vertical);
            node.rt = add(node.rt, newRect, p, !vertical);
        }

        return node;
    }

    /**
     * Add the point to the set (if it is not already in the set)
     */
    public void insert(Point2D p) {
        validateNull(p);
        this.root = add(this.root, new RectHV(0.0, 0.0, 1.0, 1.0), p, true);
    }

    private Node find(Node node, Point2D p, Boolean vertical) {
        if (node == null) return null;

        int comp = node.compareTo(p, vertical);
        if (comp < 0) return find(node.lb, p, !vertical);
        if (comp > 0) return find(node.rt, p, !vertical);
        return node;
    }

    /**
     * Does the set contain point p?
     */
    public boolean contains(Point2D p) {
        validateNull(p);
        return find(this.root, p, true) != null;
    }

    private void drawit(Node node) {
        if (node == null) return;
        drawit(node.lb);
        node.p.draw();
        drawit(node.rt);
    }

    /**
     * Draw all points to standard draw
     */
    public void draw() {
        drawit(this.root);
    }

    private void buildRange(SET<Point2D> pointInside, RectHV rect, Node node) {
        if (node == null) return;
        if (!rect.intersects(node.rect)) return;

        if (rect.contains(node.p)) {
            pointInside.add(node.p);
        }
        this.buildRange(pointInside, rect, node.lb);
        this.buildRange(pointInside, rect, node.rt);
    }

    /**
     * All points that are inside the rectangle (or on the boundary)
     */
    public Iterable<Point2D> range(RectHV rect) {
        validateNull(rect);
        SET<Point2D> pointInside = new SET<Point2D>();
        this.buildRange(pointInside, rect, this.root);
        return pointInside;
    }

    private boolean sameSide(Point2D p, Node parent, Node node, boolean vertical) {
        if (vertical)
            return Point2D.X_ORDER.compare(p, parent.p) == Point2D.X_ORDER.compare(p, node.p);

        return Point2D.Y_ORDER.compare(p, parent.p) == Point2D.Y_ORDER.compare(p, node.p);
    }

    private Point2D getNearest(Node node, Point2D p, Point2D nearest, boolean vertical) {
        if (node == null) return nearest;
        double nearestDistance = p.distanceTo(nearest);
        if (nearestDistance < node.rect.distanceTo(p)) return nearest;
        if (p.distanceTo(node.p) <= nearestDistance) {
            nearest = node.p;
        }

        Point2D subnearest;
        if (node.lb != null && sameSide(p, node, node.lb, vertical)) {
            subnearest = getNearest(node.lb, p, nearest, !vertical);
            return getNearest(node.rt, p, subnearest, !vertical);
        }

        subnearest = getNearest(node.rt, p, nearest, !vertical);
        return getNearest(node.lb, p, subnearest, !vertical);
    }

    /**
     * A nearest neighbor in the set to point p; null if the set is empty
     */
    public Point2D nearest(Point2D p) {
        validateNull(p);
        if (this.isEmpty()) return null;
        return getNearest(this.root, p, this.root.p, true);
    }

    /**
     * Unit testing of the methods (optional)
     */
    public static void main(String[] args) {
        KdTree kdTree = new KdTree();
        StdOut.println("KdTree -> Inserts");
        Point2D rootP = new Point2D(0.7, 0.2);
        kdTree.insert(rootP);
        StdOut.println(kdTree.root.p + " is equals to: " + rootP + " Rect: " + kdTree.root.rect);
        Point2D plb = new Point2D(0.5, 0.4);
        kdTree.insert(plb);
        StdOut.println(kdTree.root.lb.p + " is equals to: " + plb + " Rect: " + kdTree.root.lb.rect);
        Point2D prt = new Point2D(0.9, 0.6);
        kdTree.insert(prt);
        StdOut.println(kdTree.root.rt.p + " is equals to: " + prt + " Rect: " + kdTree.root.rt.rect);

        Point2D plblb = new Point2D(0.2, 0.3);
        kdTree.insert(plblb);
        StdOut.println(kdTree.root.lb.lb.p + " is equals to: " + plblb + " Rect: " + kdTree.root.lb.lb.rect);
        Point2D plbrt = new Point2D(0.4, 0.7);
        kdTree.insert(plbrt);
        StdOut.println(kdTree.root.lb.rt.p + " is equals to: " + plbrt + " Rect: " + kdTree.root.lb.rt.rect);

        StdOut.println("KdTree -> Contains");
        StdOut.println(rootP + " Contains the root: " + kdTree.contains(rootP));
        StdOut.println(plb + " Contains in the lb: " + kdTree.contains(plb));
        StdOut.println(prt + " Contains in the rt: " + kdTree.contains(prt));
        StdOut.println(plblb + " Contains in the lb: " + kdTree.contains(plblb));
        StdOut.println(plbrt + " Contains in the rt: " + kdTree.contains(plbrt));
        Point2D invalidPoint = new Point2D(0.1, 0.1);
        StdOut.println(invalidPoint + " is Contains? " + kdTree.contains(invalidPoint));

        StdOut.println("KdTree -> Range search...");
        RectHV rect = new RectHV(0.0, 0.0, 0.5, 0.5);
        StdOut.println("For rect: " + rect);
        for (Point2D p: kdTree.range(rect)) {
            StdOut.println("Point: " + p);
        }

        rect = new RectHV(0.2, 0.2, 0.8, 0.8);
        StdOut.println("For rect: " + rect);
        for (Point2D p: kdTree.range(rect)) {
            StdOut.println("Point: " + p);
        }

        StdOut.println("KdTree -> Nearest search...");
        Point2D p = new Point2D(1.0, 1.0);
        StdOut.println("Point nearest for " + p + " is: " + kdTree.nearest(p));
        p = new Point2D(0.5, 0.5);
        StdOut.println("Point nearest for " + p + " is: " + kdTree.nearest(p));
        p = new Point2D(0.9, 0.4);
        StdOut.println("Point nearest for " + p + " is: " + kdTree.nearest(p));

        String filename = args[0];
        In in = new In(filename);
        KdTree brute = new KdTree();
        while (!in.isEmpty()) {
            double x = in.readDouble();
            double y = in.readDouble();
            Point2D p1 = new Point2D(x, y);
            brute.insert(p1);
        }

        double x0 = 0.0, y0 = 0.0;      // initial endpoint of rectangle
        double x1 = 0.0, y1 = 0.0;      // current location of mouse
        boolean isDragging = false;     // is the user dragging a rectangle

                // draw the points
        StdDraw.clear();
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.01);
        brute.draw();
        StdDraw.show();

        // process range search queries
        StdDraw.enableDoubleBuffering();
        while (true) {

            // user starts to drag a rectangle
            if (StdDraw.isMousePressed() && !isDragging) {
                x0 = x1 = StdDraw.mouseX();
                y0 = y1 = StdDraw.mouseY();
                isDragging = true;
            }

            // user is dragging a rectangle
            else if (StdDraw.isMousePressed() && isDragging) {
                x1 = StdDraw.mouseX();
                y1 = StdDraw.mouseY();
            }

            // user stops dragging rectangle
            else if (!StdDraw.isMousePressed() && isDragging) {
                isDragging = false;
            }

            // draw the points
            StdDraw.clear();
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius(0.01);
            brute.draw();

            // draw the rectangle
            rect = new RectHV(Math.min(x0, x1), Math.min(y0, y1),
                              Math.max(x0, x1), Math.max(y0, y1));
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius();
            rect.draw();

            // draw the range search results for brute-force data structure in red
            StdDraw.setPenRadius(0.03);
            StdDraw.setPenColor(StdDraw.RED);
            for (Point2D p1 : brute.range(rect)) {
                p1.draw();
            }

            StdDraw.show();
            StdDraw.pause(20);
        }

    }
}
