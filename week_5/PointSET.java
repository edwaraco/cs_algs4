/******************************************************************************
 *  Compilation:  javac PointSet.java
 *  Execution:    java PointSet
 *  Dependencies: Point2D, RectHV
 *
 *  Brute-force to represent a set of points in the unit square.
 *
 ******************************************************************************/

import java.util.List;
import java.util.ArrayList;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.SET;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;



public class PointSET {

    private SET<Point2D> points;

    /**
     * Raise IlegallArgument exception if the object is null
     *
     * @param point Object
     */
    private static void validateNull(Object point) {
        if (point == null) {
            throw new IllegalArgumentException("Object is required");
        }
    }

    /**
     * Construct an empty set of points
     */
    public PointSET() {
        this.points = new SET<Point2D>();
    }

    /**
     * Is the set empty?
     */
    public boolean isEmpty() {
        return this.points.isEmpty();
    }

    /**
     * Number of points in the set
     */
    public int size() {
        return this.points.size();
    }

    /**
     * Add the point to the set (if it is not already in the set)
     */
    public void insert(Point2D p) {
        validateNull(p);
        this.points.add(p);
    }

    /**
     * Does the set contain point p?
     */
    public boolean contains(Point2D p) {
        validateNull(p);
        return this.points.contains(p);
    }

    /**
     * Draw all points to standard draw
     */
    public void draw() {
        for (Point2D point: this.points) {
            point.draw();
        }
    }

    /**
     * All points that are inside the rectangle (or on the boundary)
     */
    public Iterable<Point2D> range(RectHV rect) {
        validateNull(rect);
        List<Point2D> pointInside = new ArrayList<>();
        for (Point2D point: this.points) {
            if (rect.contains(point)) {
                pointInside.add(point);
            }
        }
        return pointInside;
    }

    /**
     * A nearest neighbor in the set to point p; null if the set is empty
     */
    public Point2D nearest(Point2D p) {
        validateNull(p);
        Point2D nearest = null;
        Double distance = Double.POSITIVE_INFINITY;

        for (Point2D point: this.points) {
            double dist = point.distanceTo(p);
            if (dist < distance) {
                distance = dist;
                nearest = point;
            }
        }
        return nearest;
    }

    /**
     * Unit testing of the methods (optional)
     */
    public static void main(String[] args) {
        String filename = args[0];
        In in = new In(filename);
        PointSET brute = new PointSET();
        while (!in.isEmpty()) {
            double x = in.readDouble();
            double y = in.readDouble();
            Point2D p = new Point2D(x, y);
            brute.insert(p);
        }

        double x0 = 0.0, y0 = 0.0;      // initial endpoint of rectangle
        double x1 = 0.0, y1 = 0.0;      // current location of mouse
        boolean isDragging = false;     // is the user dragging a rectangle

                // draw the points
        StdDraw.clear();
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(0.01);
        brute.draw();
        StdDraw.show();

        // process range search queries
        StdDraw.enableDoubleBuffering();
        while (true) {

            // user starts to drag a rectangle
            if (StdDraw.isMousePressed() && !isDragging) {
                x0 = x1 = StdDraw.mouseX();
                y0 = y1 = StdDraw.mouseY();
                isDragging = true;
            }

            // user is dragging a rectangle
            else if (StdDraw.isMousePressed() && isDragging) {
                x1 = StdDraw.mouseX();
                y1 = StdDraw.mouseY();
            }

            // user stops dragging rectangle
            else if (!StdDraw.isMousePressed() && isDragging) {
                isDragging = false;
            }

            // draw the points
            StdDraw.clear();
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius(0.01);
            brute.draw();

            // draw the rectangle
            RectHV rect = new RectHV(Math.min(x0, x1), Math.min(y0, y1),
                                     Math.max(x0, x1), Math.max(y0, y1));
            StdDraw.setPenColor(StdDraw.BLACK);
            StdDraw.setPenRadius();
            rect.draw();

            // draw the range search results for brute-force data structure in red
            StdDraw.setPenRadius(0.03);
            StdDraw.setPenColor(StdDraw.RED);
            for (Point2D p : brute.range(rect)) {
                p.draw();
            }

            StdDraw.show();
            StdDraw.pause(20);
        }

    }
}
